# frozen_string_literal: true

require_relative './lib/registration_listener'
require_relative './lib/wallet_controller'

RegistrationListener.start

run WalletController.new
