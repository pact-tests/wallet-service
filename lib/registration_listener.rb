# frozen_string_literal: true

require 'bunny'
require_relative './wallet'

class RegistrationListener
  class << self
    def start
      Thread.new do
        queue.subscribe(block: true) do |_delivery_info, _headers, body|
          handle(body)
        end
      end
    end

    def handle(raw_registration)
      user_id = JSON.parse(raw_registration)['user_id']
      wallet = Wallet.new(user_id)
      Wallet.add(wallet)
    end

    private

    def queue
      @queue ||= channel.queue('', exclusive: true).tap { |queue| queue.bind(exchange) }
    end

    def exchange
      @exchange ||= channel.fanout(exchange_name)
    end

    def channel
      @channel ||= connection.create_channel
    end

    def connection
      @connection ||= Bunny.new.tap(&:start)
    end

    def exchange_name
      ENV['REGISTRATION_EXCHANGE'] ||= 'registration_exchange'
    end
  end
end
