# frozen_string_literal: true

class Wallet
  class << self
    def all
      wallets.values
    end

    def find_by_user_id(user_id)
      wallets[user_id]
    end

    def add(wallet)
      wallets[wallet.user_id] = wallet
    end

    def delete_all
      wallets.clear
    end

    private

    def wallets
      @wallets ||= {}
    end
  end

  attr_reader :user_id
  attr_accessor :coins

  def initialize(user_id)
    @user_id = user_id
    @coins = 0
  end

  def to_json(*)
    { user_id: user_id, coins: coins }.to_json
  end

  alias to_s to_json
end
