# frozen_string_literal: true

require 'sinatra'
require 'sinatra/namespace'
require 'json'
require_relative './wallet'

class WalletController < Sinatra::Base
  register Sinatra::Namespace

  namespace '/api' do
    before do
      content_type 'application/json'
    end

    helpers do
      def json_params
        JSON.parse(request.body.read)
      rescue StandardError
        halt 400, { message: 'Invalid JSON' }.to_json
      end
    end

    get '/wallets' do
      Wallet.all.to_json
    end

    put '/wallets/:user_id' do |user_id|
      wallet = Wallet.find_by_user_id(user_id)
      halt 404 if wallet.nil?

      coins = json_params['coins'].to_i

      remaining_coins = wallet.coins + coins
      halt 400, { message: 'Not enough coins in wallet' }.to_json if remaining_coins.negative?

      wallet.coins = remaining_coins
      status 204
    end

    get '/wallets/:user_id' do |user_id|
      wallet = Wallet.find_by_user_id(user_id)
      halt 404 if wallet.nil?

      wallet.to_json
    end
  end
end
