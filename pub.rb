# frozen_string_literal: true

require 'bunny'
require 'json'

connection = Bunny.new
connection.start

channel = connection.create_channel
exchange = channel.fanout('registration_exchange')

user_id = ARGV.empty? ? 'Hello World!' : ARGV.join(' ')

message = { user_id: user_id }.to_json

exchange.publish(message)
puts " [x] Sent #{message}"

connection.close
