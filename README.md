Overall instructions [here](https://gitlab.com/pact-tests/rb-signup-service/-/blob/master/README.md)

## Part 2

# Run Test

```
bundle exec rspec ./spec/registration_listener_spec.rb
```

A new contract will be availabe in the folder `spec/pacts`.

# Publish a new contract

```
bundle exec rake pact:publish 
```

## Part 4

# Verify Contract

```
bundle exec rake pact:verify
```