# frozen_string_literal: true

require_relative '../lib/wallet_controller'
require_relative '../lib/wallet'
require 'rspec'
require 'rack/test'

RSpec.describe WalletController do
  include Rack::Test::Methods

  def app
    described_class.new
  end

  describe 'GET /api/wallets' do
    context 'when there are no wallets' do
      before { Wallet.delete_all }

      it 'returns an empty array' do
        get '/api/wallets'

        expect(last_response).to be_ok
        expect(last_response.content_type).to eq('application/json')
        expect(last_response.body).to eq('[]')
      end
    end

    context 'when wallets are present' do
      let(:user_id) { '123' }
      let(:wallet) { Wallet.new(user_id) }
      before { Wallet.add(wallet) }

      it 'returns them' do
        get '/api/wallets'

        expect(last_response).to be_ok
        expect(last_response.content_type).to eq('application/json')
        expect(last_response.body).to eq('[{"user_id":"123","coins":0}]')
      end
    end
  end
end