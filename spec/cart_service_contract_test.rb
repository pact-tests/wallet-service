# frozen_string_literal: true

require 'pact/provider/rspec'
require_relative '../lib/wallet'

Pact.service_provider 'Wallet Service' do
  publish_verification_results true
  app_version '1.0'

end
